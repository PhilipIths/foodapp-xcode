//
//  FoodList.m
//  FoodApp
//
//  Created by Stjernström on 2015-02-23.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "FoodList.h"
#import "ProductInfo.h"

@interface FoodList ()

@end

@implementation FoodList

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return self.list.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectedCell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.list[indexPath.row][@"name"];
    
    //NSLog(@"indexPath: %d", indexPath.row);
    
    return cell;
}

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    ProductInfo *info = [segue destinationViewController];
    
    UITableViewCell *cell = sender;
    
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    NSInteger row = path.row;
    
    info.productNumber = self.list[row][@"number"];
    
    NSLog(@"ProductNumber: %@", info.productNumber);
}


@end
