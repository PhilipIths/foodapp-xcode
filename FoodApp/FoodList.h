//
//  FoodList.h
//  FoodApp
//
//  Created by Stjernström on 2015-02-23.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodList : UITableViewController

@property (weak, nonatomic) NSString *search;
@property (weak, nonatomic) NSArray *list;

@end
