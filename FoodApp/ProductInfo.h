//
//  ProductInfo.h
//  FoodApp
//
//  Created by Stjernström on 2015-02-25.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductInfo : UIViewController

@property (nonatomic) NSString *productNumber;

@end
