//
//  ProductInfo.m
//  FoodApp
//
//  Created by Stjernström on 2015-02-25.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ProductInfo.h"

@interface ProductInfo ()

@property (nonatomic) NSString *getProductInfo;
@property (nonatomic) NSString *nutrition;
@property (nonatomic) NSDictionary *values;

@property (weak, nonatomic) IBOutlet UILabel *Name;
@property (weak, nonatomic) IBOutlet UILabel *Cholesterol;
@property (weak, nonatomic) IBOutlet UILabel *Fibres;
@property (weak, nonatomic) IBOutlet UILabel *Iron;
@property (weak, nonatomic) IBOutlet UILabel *Protein;
@property (weak, nonatomic) IBOutlet UILabel *Salt;
@property (weak, nonatomic) IBOutlet UILabel *Water;
@property (weak, nonatomic) IBOutlet UILabel *Zink;
@property (weak, nonatomic) IBOutlet UILabel *NutritionValue;


@end

// The AutoLayout isn't perfect, considering that I had to use the auto-resolve on (Name) to get it to not mess up.

// I might need some more help with figuring it out, because this isn't very intuitive.

@implementation ProductInfo

// When the view is loaded, a session of matapi.se/foodstuff is created, where the method gets variables and applies them to the layouts labels.

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.getProductInfo = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.productNumber];
    
    NSLog(@"Number: %@", self.productNumber);
    
    NSURL *url = [NSURL URLWithString:self.getProductInfo];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                if(error) {
                                                    NSLog(@"Request error: %@", error);
                                                }
                                                
                                                NSError *parsingError;
                                                
                                                self.values = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                                                NSLog(@"self.values = %@", self.values);
                                                
                                                if (!parsingError) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.Name.text = self.values[@"name"];
                                                        NSLog(@"ValueNumber: %@", self.values[@"number"]);
                                                        
                                                        self.Cholesterol.text = [NSString stringWithFormat:@"Cholesterol: %@", self.values[@"nutrientValues"][@"cholesterol"]];
                                                        
                                                        self.Fibres.text = [NSString stringWithFormat:@"Fibres: %@", self.values[@"nutrientValues"][@"fibres"]];
                                                        
                                                        self.Iron.text = [NSString stringWithFormat:@"Iron: %@", self.values[@"nutrientValues"][@"iron"]];
                                                        
                                                        self.Protein.text = [NSString stringWithFormat:@"Protein: %@", self.values[@"nutrientValues"][@"protein"]];
                                                        
                                                        self.Salt.text = [NSString stringWithFormat:@"Salt: %@", self.values[@"nutrientValues"][@"salt"]];
                                                        
                                                        self.Water.text = [NSString stringWithFormat:@"Water: %@", self.values[@"nutrientValues"][@"water"]];
                                                        
                                                        self.Zink.text = [NSString stringWithFormat:@"Zink: %@", self.values[@"nutrientValues"][@"zink"]];
                                                        
                                                        [self getNutrition];
                                                        });
                                                    
                                                } else {
                                                    NSLog(@"Couldn't parse json: %@", parsingError);
                                                }
                                            }];
    
    [task resume];
    [self fadeInLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// fadeInLayout gives most of the objects in the layout a gradual fade-in from top to bottom when the layout is loaded.

- (void)fadeInLayout {
    
    self.Name.alpha = 0.0;
    self.Cholesterol.alpha = 0.0;
    self.Fibres.alpha = 0.0;
    self.Iron.alpha = 0.0;
    self.Protein.alpha = 0.0;
    self.Salt.alpha = 0.0;
    self.Water.alpha = 0.0;
    self.Zink.alpha = 0.0;
    self.NutritionValue.alpha = 0.0;
    
    double duration = 0.5;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.5];
    
    self.Name.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.55];
    
    self.Cholesterol.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.6];
    
    self.Fibres.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.65];
    
    self.Iron.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.7];
    
    self.Protein.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.75];
    
    self.Salt.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.8];
    
    self.Water.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.85];
    
    self.Zink.alpha = 1.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelay:0.9];
    
    self.NutritionValue.alpha = 1.0;
    
    [UIView commitAnimations];
}

- (void)getNutrition {
    
    float value = 0;
    
    if (self.values[@"nutrientValues"][@"cholesterol"] != 0) {
        NSNumber *getVal = self.values[@"nutrientValues"][@"cholesterol"];
        float temp = getVal.floatValue * -1;
        value += temp;
    }
    
    if (self.values[@"nutrientValues"][@"fibres"] != 0) {
        NSNumber *getVal = self.values[@"nutrientValues"][@"fibres"];
        float temp = getVal.floatValue;
        value += temp;
    }
    
    if (self.values[@"nutrientValues"][@"iron"] != 0) {
        NSNumber *getVal = self.values[@"nutrientValues"][@"iron"];
        float temp = getVal.floatValue / 10;
        value += temp;
    }
    
    if (self.values[@"nutrientValues"][@"protein"] != 0) {
        NSNumber *getVal = self.values[@"nutrientValues"][@"protein"];
        float temp = getVal.floatValue * 0.8;
        value += temp;
    }
    
    if (self.values[@"nutrientValues"][@"salt"] != 0) {
        NSNumber *getVal = self.values[@"nutrientValues"][@"salt"];
        float temp = getVal.floatValue / 10;
        value += temp;
    }
    
    if (self.values[@"nutrientValues"][@"water"] != 0) {
        NSNumber *getVal = self.values[@"nutrientValues"][@"water"];
        float temp = getVal.floatValue / 2;
        value += temp;
    }
    
    if (self.values[@"nutrientValues"][@"zink"] != 0) {
        NSNumber *getVal = self.values[@"nutrientValues"][@"zink"];
        float temp = getVal.floatValue / 10;
        value += temp;
    }
    
    self.NutritionValue.text = [NSString stringWithFormat:@"Nutritional value: %f", value];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
