//
//  ViewController.m
//  FoodApp
//
//  Created by Stjernström on 2015-02-20.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewController.h"
#import "FoodList.h"

@interface ViewController ()

@property (strong, nonatomic) NSString *searchString;
@property (strong, nonatomic) NSArray *root;

@property (weak, nonatomic) IBOutlet UILabel *mainText;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIButton *SearchButton;

@property (nonatomic) IBOutlet UIImageView *firstFood;
@property (nonatomic) IBOutlet UIImageView *secondFood;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIAttachmentBehavior *attachment;
@property (nonatomic) UISnapBehavior *snap;
@property (nonatomic) UIDynamicItemBehavior *behavior;


@end

@implementation ViewController

// onSearch gets the value from *searchField to go to a specific URL in matapi.se, creates a request and session, parses a list from the session to *root, and performs a segue to send *root and *searchString to the class "FoodList".

- (IBAction)onSearch:(id)sender {
    
    self.searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", self.searchField.text];
    
    NSLog(@"Search with string: %@", self.searchString);
    
    NSURL *url = [NSURL URLWithString:self.searchString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                            if (self.searchField.text.length != 0) {
                                                    
                                if(error) {
                                    NSLog(@"Request error: %@", error);
                                }
                                
                                NSError *parsingError;
                                
                                self.root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                                NSLog(@"self.root = %@", self.root);
                                
                                if (!parsingError) {
                                    //NSArray *relatedTopics = self.root[@"RelatedTopics"];
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        assert(self.root);
                                        [self performSegueWithIdentifier:@"SearchList" sender:self];
                                        
                                        /*if (relatedTopics.count > 0) {
                                         self.result.text = relatedTopics[0][@"Text"];
                                         }
                                         
                                         else {
                                         self.result.text = @"No topics found.";
                                         }*/
                                    });
                                    
                                } else {
                                    NSLog(@"Couldn't parse json: %@", parsingError);
                                }
                            }
                        }];
    
    [task resume];
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fadeInLayout];
    [self generateGravity];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// fadeInLayout gives most of the objects in the layout a fade-in when the layout is loaded.

- (void)fadeInLayout {
    
    self.mainText.alpha = 0.0;
    self.searchField.alpha = 0.0;
    self.SearchButton.alpha = 0.0;
    //self.firstFood.alpha = 0.0;
    //self.secondFood.alpha = 0.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.8];
    [UIView setAnimationDelay:0.5];
    //[UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    self.mainText.alpha = 1.0;
    self.searchField.alpha = 1.0;
    self.SearchButton.alpha = 1.0;
    //self.firstFood.alpha = 1.0;
    //self.secondFood.alpha = 1.0;
    
    [UIView commitAnimations];
}

// generateGravity gives the objects *firstFood and *secondFood gravity and collision in the layout.

- (void)generateGravity {
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.firstFood, self.secondFood]];
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.firstFood, self.secondFood]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
}

- (IBAction)onTap:(id)sender {
    
    CGPoint pos = [sender locationInView:self.view];
    
    // Tap bottom half, normal gravity
    if (pos.y > self.view.center.y) {
        self.gravity.gravityDirection = CGVectorMake(0, 1.0);
    }
    
    // Tap top half, reverse gravity
    else if (pos.y < self.view.center.y) {
        self.gravity.gravityDirection = CGVectorMake(0, -1.0);
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    FoodList *food = [segue destinationViewController];
    
    food.search = self.searchString;
    
    assert(self.root);
    food.list = self.root;
}

@end
